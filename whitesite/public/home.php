<?php

ini_set('session.cookie_lifetime', 30); // Куки сессии живут 30 секунд
ini_set('session.gc_maxlifetime', 30); // Данные сессии на сервере живут 30 секунд

session_start();

// Проверяем, был ли пользователь авторизован и истекло ли время сессии
if (isset($_SESSION['auth_time']) && (time() - $_SESSION['auth_time']) > 30) {
    // Время сессии истекло, разлогиниваем пользователя
    $_SESSION = array(); // Очищаем массив сессии
    session_destroy(); // Уничтожаем сессию
    setcookie(session_name(), "", time() - 3600, "/"); // Удаляем куки сессии
    header("Location: index.php"); // Перенаправляем пользователя на страницу входа
    exit();
}

$is_admin = $_SESSION['is_admin'] ?? false;

?>
    <h1>Welcome, <?php echo htmlspecialchars($_SESSION['username']); ?></h1>
    <p>This is your home page.</p>

<?php if ($is_admin): ?>
    <p><strong>You are an administrator with unlimited rights.</strong></p>
<?php endif; ?>

<a href="comments.php">Leave a comment</a>


