<?php

ini_set('session.cookie_lifetime', 30);
ini_set('session.gc_maxlifetime', 30);

session_start();

// Обновляем время последнего взаимодействия с сайтом при каждом запросе
if (!isset($_SESSION['auth_time'])) {
    $_SESSION['auth_time'] = time();
} else {
    if ((time() - $_SESSION['auth_time']) > 30) {
        // Время сессии истекло, разлогиниваем пользователя
        $_SESSION = array();
        session_destroy();
        setcookie(session_name(), "", time() - 3600, "/");
        header("Location: index.php");
        exit();
    } else {
        // Сессия еще действительна, обновляем время последнего взаимодействия
        $_SESSION['auth_time'] = time();
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $host = 'db';
    $db   = 'white_database';
    $user = 'root';
    $pass = 'white_password';
    $charset = 'utf8mb4';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $options = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];

    try {
        $pdo = new PDO($dsn, $user, $pass, $options);
    } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }

    $username = $_POST['username'];
    $password = $_POST['password'];

    $stmt = $pdo->prepare("SELECT * FROM users WHERE username = ?");
    $stmt->execute([$username]);
    $user = $stmt->fetch();

    if ($user && password_verify($password, $user['password'])) {
        // Пользователь успешно авторизован, устанавливаем время авторизации
        $_SESSION['auth_time'] = time(); // Запоминаем время авторизации
        $_SESSION['is_admin'] = $user['is_admin'] === 1;
        $_SESSION['username'] = $username;
        header("Location: home.php");
        exit;
    } else {
        echo "Invalid login or password.";
    }
}
?>

<form method="post">
    Username: <input type="text" name="username"><br>
    Password: <input type="password" name="password"><br>
    <input type="submit" value="Login">
</form>

