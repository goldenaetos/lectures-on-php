jQuery(document).ready(function () {

  /* Desktop Navbar Animations
 ***********************************************************************/
  // Gets host from window.location
  const beta = location.host.includes('postman-beta') ? '-beta' : '';
  
  function getCookie(a) {
    if (typeof document !== 'undefined') {
      const b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
      return b ? b.pop() : '';
    }
    return false;
  }
  const cookie = getCookie('getpostmanlogin');

  // Checks if login cookie exists and changes link in uber-nav from 'sign in' to 'launch postman'. 
  // Hides 'register' button if signed in
  
  if (cookie !== 'yes') {
    jQuery(".pingdom-transactional-check__sign-in-button").removeClass('d-none').attr("href", 'https://identity.getpostman' + beta + '.com/login?continue=https%3A%2F%2Fgo.postman.co%2Fbuild');
    jQuery(".pingdom-transactional-check__register-button").removeClass('d-none').attr("href", 'https://identity.getpostman' + beta + '.com/signup?continue=https%3A%2F%2Fgo.postman.co%2Fbuild');
  }
  else {
    jQuery(".pingdom-transactional-check__launch-button").removeClass('d-none').attr("href", 'https://go.postman' + beta + '.co/build');
  }

  /* add class to hrefs with images to hide orange bar
 ***********************************************************************/
  jQuery('img').parent('.entry-content a').addClass('no-border');

  /* Navbar animations
   ***********************************************************************/

  // Toggles Caret Menu to turn on click
    jQuery(".mobile-icon-button_caret").bind("click", function () {
      jQuery(".icon-caret").toggleClass("open");
    });
  
  jQuery('#secondaryNav').on('click', function () {
    jQuery('body').toggleClass('menu-open');
    jQuery('.nav-primary').toggleClass('activeMenu');
    jQuery('.nav-secondary').toggleClass('activeMenu');
  })

  function showBsDropdown() {
    jQuery(this)
      .find('.dropdown-menu')
      .first()
      .stop(true, true)
      .fadeToggle(250);
    jQuery(this)
      .find('.arrow-icon')
      .addClass('show');
  }
  jQuery('.dropdown').on('show.bs.dropdown', showBsDropdown);
  function hideBsDropdown() {
    jQuery(this)
      .find('.dropdown-menu')
      .stop(true, true)
      .fadeToggle(250);
    jQuery(this)
      .find('.arrow-icon')
      .removeClass('show');
  }
  jQuery('.dropdown').on('hide.bs.dropdown', hideBsDropdown);

  function toggleGlobalNav() {
    // Global Mobile Icon Transition
    const toggler = document.getElementById('globalNav').getAttribute('aria-expanded');
    const body = document.querySelector('body');
    const icon1 = document.querySelector('#icon-wrap-one');
    if (toggler === 'false') {
      body.classList.add('lock');
      icon1.classList.add('open');
    } else {
      icon1.classList.remove('open');
      body.classList.remove('lock');
      const icon2 = document.getElementById('navbar-chevron-icons');
      const togglerSecondary = document
        .querySelector('#secondaryNav.navbar-toggler')
        .getAttribute('aria-expanded');
      if (togglerSecondary === 'false') {
        icon2.classList.remove('open');
      }
    }
  }

  jQuery('#globalNav').bind('click', toggleGlobalNav);

  function toggleLcNav() {
    // LC Mobile Icon Transition
    const toggler = document.getElementById('secondaryNav').getAttribute('aria-expanded');
    const toggleChevron = document.getElementById('navbar-chevron-icons');

    if (toggler === 'false') {
      toggleChevron.classList.add('open');
    } else {
      toggleChevron.classList.remove('open');
    }
  }
  jQuery('#secondaryNav').bind('click', toggleLcNav);
  //  animations finish


  // Opens mobile search form
  jQuery(".mobile-search-button").bind("click", function () {
    const nav = document.querySelector('.mobile-nav');
    nav.classList.add('d-none');

    const d = document.querySelector('.mobile-search_nav');
    d.classList.remove('d-none');
  });
  // Closes search form
  jQuery(".mobile-search_close-button").bind("click", function () {

    const nav = document.querySelector('.mobile-nav');
    nav.classList.remove('d-none');

    const d = document.querySelector('.mobile-search_nav');
    d.classList.add('d-none');
  });

/* START - Google FAQ Schema functions for SEO */

// checks for H2 element
function findQuestion(header, index) {
  let question;
  if (
    header[index].tagName === 'H2' &&
    header[index].innerText.slice(-1) === '?'
  ) {
    question = `${header[index].innerText}`;
  }
  return question;
}

// checks for paragraph tag next to H2 or nested div
function findAnswer(header, index) {
  let answer;
  if (
    header[index].nextElementSibling &&
    header[index].nextElementSibling.tagName === 'P'
  ) {
    answer = `${header[index].nextElementSibling.innerText}`;
  } else if (
    header[index].nextElementSibling &&
    header[index].nextElementSibling.tagName === 'DIV' &&
    header[index].nextElementSibling.firstChild.tagName === 'P'
  ) {
    answer = `${header[index].nextElementSibling.firstChild.innerText}`;
  }
  return answer;
}

(function generateFAQSchema() {
  if (typeof document !== 'undefined') {
    const schema = {
      '@context': 'https://schema.org',
      '@type': 'FAQPage',
      mainEntity: []
    };
    // get all divs with a child h2 element and filter
    const headers = document.querySelectorAll('div > h2');
    for (let index = 0; index < headers.length; index += 1) {
      const hasQuestion = findQuestion(headers, index);
      const hasAnswer = findAnswer(headers, index);
      if (hasQuestion && hasAnswer) {
        // generate schema and push into an array
        const faqObj = {
          '@type': 'Question',
          name: hasQuestion,
          acceptedAnswer: {
            '@type': 'Answer',
            text: hasAnswer
          }
        };
        schema.mainEntity.push(faqObj);
      }
    }

    // Google limits search results to 7
    if (schema.mainEntity.length >= 8) {
      schema.mainEntity.length = 7;
    }

    // load script
    if (schema.mainEntity.length >= 2) {

    // // ********* for testing ************
    // console.log(schema);
      
    const script = document.createElement('script');
    script.setAttribute('type', 'application/ld+json');
    script.textContent = JSON.stringify(schema);
    document.head.appendChild(script);
    }
  }
}())

  /* END - Google FAQ Schema functions for SEO */

/* pmtSDK
***********************************************************************/
window.pmt=function(){var t={version:"v2.0.21",log:function(e){t.output=t.output||[],t.output.push(e)},set:function(e,o){t[e]=o},getPubId:function(){return(document.cookie.match("(^|;) ?_PUB_ID=([^;]*)(;|$)")||[])[2]},drivePubId:function(e){const o=window.location.href,n="pub_id=";let a,c;if(o.match(n)){if(c=o.split(n).pop().split("&").shift(),a="_PUB_ID="+c+"; path=/",document.cookie=a,e){let t=o.replace(n+c,"");t=t.replace("?&","?"),t=t.replace("&&","&");t.split("?").pop()||(t=t.split("?").shift());const e=t.length-1;"&"===t.charAt(e)&&(t=t.substring(0,e)),window.location.replace(t)}return a}return t.getPubId()},driveCampaignId:function(t){let e;const o="dcid=",n=t&&t.dcid||window.location.search&&window.location.search.match(o)&&window.location.search.split(o).pop().split("&").shift()||(document.cookie.match("(^|;) ?dcid=([^;]*)(;|$)")||[])[2];let a,c;const i=t&&t.form,r=t&&t.url||window.location.href;return function(t){const e=t;let a;const c=n&&n.replace(o,"");t&&(e.tagName?n&&!e.driver_campaign_id&&(a=document.createElement("input"),a.type="hidden",a.name="driver_campaign_id",a.value=c,e.appendChild(a)):n&&(e.driver_campaign_id=c))}(i),r.match(o)?(a=r.split(o).pop().split("&").shift(),c=new Date,c.setDate(c.getDate()+30),e="dcid="+a+"; expires="+c.toUTCString()+"; path=/",document.cookie=e,e):t},enablePostmanAnalytics:function(e,o,n){if("function"!=typeof e||e.postmanAnalyticsEnabled||navigator.doNotTrack&&!o._disableDoNotTrack)return e;function a(t){return t.replace(/"/gi,'"')}function c(t){return"string"==typeof t&&t.split(window.location.host).pop()}return o||(o={}),e.postmanAnalyticsEnabled=!0,function(i,r,d,s){const l="load"!==r||window.location.href!==t.currentURL;if(!l)return!1;e.apply(this,arguments);const p="gtm.uniqueEventId";let u,m,f,g;const h=r||n;t.initCategory||(t.initCategory=i);const w={category:i,action:h,indexType:"client-events",property:o._property||document.location.host,propertyId:document.location.host,traceId:t.traceId||o._traceId||"anonymous",timestamp:(new Date).toISOString()},y=c(t.currentURL)||document.referrer||t.externalURL||"",b=navigator.language||window.navigator.userLanguage||"?";w.meta={url:c(y),language:b},d&&(w.entityId=d),"load"===w.action&&w.entityId&&document.body&&document.body.id&&(w.entityId=w.entityId+"#"+document.body.id),s&&(u=parseInt(s,10),m=u&&!u.isNaN&&u||null,g="string"==typeof s,f=g&&s.match(":")&&s.split(":").pop()||g&&s||"object"==typeof s&&a(JSON.stringify(s))||"",m&&(w.value=m),f&&(d?w.entityId+=":"+f:w.entityId=f));const k=Object.keys(o)||[];function I(t){const e=new XMLHttpRequest;e.open("POST",o._url),e.setRequestHeader("Accept","application/json"),e.setRequestHeader("Content-type","text/plain"),e.send(t)}function _(t,e){const o=t&&t.split(",")||[],n=o.length;let a,c;for(a=0;a<n;a+=1){const t=o[a];if(c=-1!==e.indexOf(t),c)break}return c}k.forEach((function(t){"_"!==t.charAt(0)&&(w[t]=o[t])})),r||"object"!=typeof i||(w.action=i.action||i.event||i[Object.keys(i)[0]],i[p]&&(w.category=p+"-"+i[p])),"local"===w.env&&(w.env="beta"),"object"==typeof w.category&&w.category&&"string"==typeof w.category.category&&(w.category=w.category.category),["category","event","label"].forEach((function(t){"object"==typeof w[t]&&(w[t]=w[t]&&a(JSON.stringify(w[t])))})),w.userId=t.getPubId()||(document.cookie.match("(^|;) ?_pm=([^;]*)(;|$)")||[])[2]||w.userId,t.userId=w.userId;const v=t.traceId.split("|").pop();return t.traceId=t.traceId.split(v).shift()+t.userId,-1===window.name.indexOf("PM.")&&(window.name=t.traceId),t.api().store(),w.category&&w.action&&"function"==typeof o.fetch&&o.fetch(o._url,w)||w.entityId&&"object"==typeof document&&(()=>{const e=o._allow&&_(o._allow,document.location.pathname)||!o._allow&&!0,n=o._disallow&&_(o._disallow,document.location.pathname),a=btoa(JSON.stringify(w));if(e&&!n){if(fetch){if("load"===w.action){if(w.action&&!l)return t.trackIt(),!1;w.entityId=w.entityId.split("#").shift()}t.traceId&&(w.traceId=t.traceId),fetch(o._url,{method:"POST",headers:{Accept:"text/html","Content-Type":"text/html"},body:a,mode:"no-cors",keepalive:!0}).catch((function(e){t.log(e)})),t.event=w}else I(a);t.currentURL=window.location.href,-1===w.meta.url.indexOf(document.location.host)&&(t.externalURL=w.meta.url)}})(),!0}},ga:function(){"function"==typeof window.ga&&window.ga.apply(this,arguments)},getEnv:function(t){let e;e="production";const o=t||document.location.hostname;return["beta","local","stag"].forEach((function(t){o.match(t)&&(e=t)})),e},setScalp:function(e){if("object"==typeof window){const o=(document.location.search&&document.location.search.match("dcid=([^;]*)(;|$)")||[])[1],n=o&&o.split("&").shift()||(document.cookie.match("(^|;) ?dcid=([^;]*)(;|$)")||[])[2],a=document.location.search.substr(1).split("&"),c=window.localStorage.getItem("utms"),i=c&&c.split(",")||[];a.forEach((t=>{const e=t.match("([UTM|utm].*)=([^;]*)(;|$)");e&&(-1!==e[0].indexOf("utm")||-1!==e[0].indexOf("UTM"))&&i.push(e[0])}));const r=document.location.host.split("."),d=r.pop(),s=r.pop(),l=i.length&&i.join(".")||"",p="PM.",u=p+btoa((new Date).toISOString()),m=window.name&&window.name.match("|PM.")&&window.name.split("|").pop()||(document.cookie.match("(^|;) ?_pm=([^;]*)(;|$)")||[])[2],f=function(t){const e=new Date;return e.setDate(e.getDate()+1080),"_pm="+t+"; expires="+e.toUTCString()+"; domain=."+s+"."+d+"; path=/"};(function(t){const e=-1!==document.cookie.indexOf("_pm"),o=-1===t.indexOf(p),n=-1!==document.cookie.indexOf(p);(!e||o||!window.location.hostname.match(/\.postman.com/)&&n)&&(document.cookie=f(t))})(m||u);const g="pm"+btoa((new Date).getTime());"string"==typeof window.name&&"pm"===window.name.substring(0,2)||(n&&-1===window.name.indexOf("DCID.")?window.name=g+"|DCID."+n+(l&&"|"+l||"")+"|"+(m||u):window.name=g+(l&&"|"+l||"")+"|"+(m||u));const h=window.parent&&window.parent.name||window.name,w=function(){t.scalpCount||(t.scalpCount=0),t.scalpCount+=1},y=t.getPubId()||m||window.name.split("|").pop(),b={env:"function"==typeof t.getEnv&&t.getEnv()||"production",type:"events-website",userId:y,_allow:!e.disallow&&e.allow,_disableDoNotTrack:void 0===e.disableDoNotTrack||e.disableDoNotTrack,_disallow:!e.allow&&e.disallow,_property:e.property||document.location.host,_traceId:h},k=b.env.match("prod")?"https://bi.pst.tech/events":"https://bi-beta.pst.tech/events";b._url=e.url||k,document.cookie="_pm.traceId="+h+"; domain=."+s+"."+d+"; path=/",t.scalp=t.enablePostmanAnalytics(w,b),t.traceId=h,t.userId=y}},getTraceUrl:function(e){const o=-1!==e.indexOf("?")?"&":"?";return e+o+"trace="+t.traceId},trace:function(e){document.location.href=t.getTraceUrl(e)},getUtmUrl:function(e){const o=-1!==e.indexOf("?")?"&":"?",n=t.traceId.split(".").pop(),a=t.traceId.split("."+n).shift().substr(1).split("."),c=[];return a.forEach((t=>{const e=t.match("([UTM|utm].*)=([^;]*)(;|$)");e&&(-1!==e[0].indexOf("utm")||-1!==e[0].indexOf("UTM"))&&c.push(e[0])})),e+o+(c.length&&c.join("&")||"utm="+document.location.host)},utm:function(e){let o=t.getUtmUrl(e);o.match("trace=")||(o=o+"&trace="+t.traceId),document.location.href=o},trackClicks:function(e,o){const n=function(n){const a=document.body&&document.body.id&&"#"+document.body.id||"";if(e){const c=n.target.getAttribute(e);c&&t.scalp(o||t.initCategory,"click","target",a+c)}else if(!e&&("string"==typeof n.target.className||"string"==typeof n.target.id)){const e=n.target.className||n.target.id||n.target.parentNode.className||-1;if("string"==typeof e){const c=document.location.pathname+a+":"+n.target.tagName+"."+e.split(" ").join("_");try{t.scalp(o||t.initCategory,"click",c)}catch(e){t.log(e.message)}}}};document.body.getAttribute("data-trackClicks")||document.body.addEventListener("mousedown",n,!0),document.body.setAttribute("data-trackClicks",e||"default")},driveTrack:function(e){let o;const n="_track=",a=e&&e._track||window.location.search&&window.location.search.match(n)&&window.location.search.split(n).pop().split("&").shift()||(document.cookie.match("(^|;) ?_track=([^;]*)(;|$)")||[])[2],c=e&&e.url||window.location.href,i=t.getEnv(),r=i.match("stag")?"stage":i;return t.tracking=!0,t.trackIt(),c.match(n)?(o="postman-"+r+".track="+a+"; path=/",document.cookie=o,o):e},trackIt:function(){const e=(document.cookie.match("(^|;) ?postman-[a-z]+.track=([^;]*)(;|$)")||[])[2];if(e&&t.tracking){let t=document.location.href;const o=-1===t.indexOf("?")?"?":"&";-1===t.indexOf("_track")&&"default"!==e&&(t=`${t}${o}_track=${e}`,document.location.replace(t))}},xhr:function(t,e){const o=new XMLHttpRequest,n="t="+(new Date).getTime(),a=-1===t.indexOf("?")?"?":"&",c=t+a+n;o.withCredentials=!0,o.addEventListener("readystatechange",(function(){4===this.readyState&&e(this.responseText)})),o.open("GET",c),o.send()},bff:function(e,o,n){const a=(n?"/mkapi/":"https://www.postman.com/mkapi/")+e+".json";t.xhr(a,o)},store:function(e,o){const n=(document.cookie.match("(^|;) ?_pm.store=([^;]*)(;|$)")||[])[2],a=n&&JSON.parse(n)||{};t.stored={...a},e&&o&&(t.stored[e]=o);const c=document.location.host.split("."),i=c.pop(),r=c.pop(),d=new Date;d.setDate(d.getDate()+1080);let s="_pm.store="+JSON.stringify(t.stored)+"; expires="+d.toUTCString()+"; domain=."+r+"."+i+"; path=/";if(!r){const t=s.split("domain=").pop().split(";").shift();s=s.replace(t,"localhost")}document.cookie=s},api:function(e){"object"==typeof e&&Object.keys(e).forEach((function(t){window[t]=e[t]}));const o=window.pm,n=o&&o.billing,a=n&&n.plan,c=a&&a.features;if(c){const e=c&&c.is_paid_plan_growth,o=e&&e.value,n=c&&c.is_enterprise_plan_growth,a=(n&&n.value?"enterprise":o&&"paid")||"free";t.store("plan",a)}return t}};return setTimeout((function(){const t=document.getElementById("pmtSDK"),e=t&&t.getAttribute("data-track-category")||"pm-analytics",o=t&&t.getAttribute("data-track-property"),n=t&&t.getAttribute("data-track-url"),a=t&&"false"!==t.getAttribute("data-track-disable-do-not-track"),c=t&&"true"===t.getAttribute("data-drive-track"),i=t&&"false"!==t.getAttribute("data-drive-campaign-id"),r=t&&"false"!==t.getAttribute("data-drive-pub-id"),d=t&&"false"!==t.getAttribute("data-track-load"),s=t&&"false"!==t.getAttribute("data-track-clicks"),l=s&&t.getAttribute("data-track-clicks-attribute")||null;if(o){const t={property:o};n&&(t.url=n),a&&(t.disableDoNotTrack=a),window.pmt("setScalp",[t]),d&&window.pmt("scalp",[e,"load",document.location.pathname]),s&&window.pmt("trackClicks",[l,e]),i&&window.pmt("driveCampaignId"),r&&window.pmt("drivePubId",[!0]),c&&window.pmt("driveTrack")}}),1e3),function(e,o){return t[e]?"function"==typeof t[e]?t[e].apply(t,o):t[e]:null}}();


/* pmtConfig
***********************************************************************/
window.pmt('setScalp', [{ property: 'marketing-blog-theme' }]);
window.pmt('scalp', ['pm-analytics', 'load', document.location.pathname]);
window.pmt('trackClicks', []);

}); // end document ready