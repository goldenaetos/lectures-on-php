

function deepCopy(obj) {
    let copy;

    if (obj instanceof Date) {
        return new Date(obj.getTime());
    }

    copy = Array.isArray(obj) ? [] : {};

    for (let key in obj) {
        const value = obj[key];
        copy[key] = (typeof value === "object" && value !== null) ? deepCopy(value) : value;
    }

    return copy;
}

const user = {
    name: "Alex",
    passport: {
        number: '11111111111',
        date: new Date(),
    },
};

const userCopy = deepCopy(user);

console.log(userCopy);
console.log(user.passport.date === userCopy.passport.date);
console.log(user.passport.date.getTime() === userCopy.passport.date.getTime());

