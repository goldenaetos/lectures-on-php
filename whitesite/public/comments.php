<?php

$cookieLifetime = 30; // Cookie lifetime in seconds

// Set session cookie parameters before starting the session
session_set_cookie_params($cookieLifetime);

session_start();

// Update session cookies with new expiration time on every request
if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), session_id(), time() + $cookieLifetime, "/");
}

// Check and update the time of the last user activity in the session
if (!isset($_SESSION['LAST_ACTIVITY'])) {
    $_SESSION['LAST_ACTIVITY'] = time();
} elseif (time() - $_SESSION['LAST_ACTIVITY'] > $cookieLifetime) {
    // If more time has passed since the last activity than $cookieLifetime, log out the user
    $_SESSION = array();
    session_destroy();
    setcookie(session_name(), "", time() - 3600, "/");
    header("Location: index.php");
    exit();
} else {
    $_SESSION['LAST_ACTIVITY'] = time(); // Update last activity time
}

if (!isset($_SESSION['username'])) {
    header('Location: index.php');
    exit();
}

$host = 'db';
$db   = 'white_database';
$user = 'root';
$pass = 'white_password';
$charset = 'utf8mb4';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

try {
    $pdo = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['comment'])) {
    $comment = $_POST['comment'];
    $stmt = $pdo->prepare("INSERT INTO comments (username, comment) VALUES (?, ?)");
    $stmt->execute([$_SESSION['username'], $comment]);
}

$stmt = $pdo->query("SELECT username, comment FROM comments ORDER BY id DESC");
$comments = $stmt->fetchAll();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Comments</title>
</head>
<body>
<h1>Welcome, <?php echo htmlspecialchars($_SESSION['username']); ?></h1>

<form method="post">
    <textarea name="comment"></textarea><br>
    <input type="submit" value="Post Comment">
</form>

<h2>Comments:</h2>
<?php foreach ($comments as $comment): ?>
    <p><strong><?php echo htmlspecialchars($comment['username']); ?>:</strong> <?php echo $comment['comment']; ?></p>
<?php endforeach; ?>
</body>
</html>

